#!/bin/bash

set -e

python manage.py collectionstatic --noinput
python manage.py wait_for_db
python manage.py migrate

uwsgi --socket :9000 --workeers 4 --master --enable-threads --module app.wsgi
